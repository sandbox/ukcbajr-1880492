<?php
/*****************************************************************
 * @file
 * Class that extends the views plug-in module. Creates a new
 * view type that uses jrbCanvas and / or enables inline editing
 * of data
 *
 * John Bell 2012
 * jrbcharts.com
 */

class jrbchartsview_views_plugin_style_chart extends views_plugin_style {

  /**
   * Set default options.
   */

  function options(&$options) {
    parent::options($options);
    // Get the default chart values
    $options['format']['classtext'] = 'table-scroll';
    $options['format']['headerrows'] = 1;
    $options['format']['headercols'] = 0;
    $options['format']['node_id'] = '';
    $options['format']['node_edit'] = '';
    $options['format']['use_canvas'] = 'Yes';
    $options['format']['makeeditable'] = 'No';
    $options['sticky'] = 0;

  }

  /**
   * Generate a form for setting options.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $fieldLabels = $this->display->handler->get_field_labels();
    array_unshift($fieldLabels, '');

    $form['format'] = array(
      '#type' => 'fieldset',
      '#title' => t('Format Options'),
      '#description' => t('Options to define how data is presented'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    $form['format']['classtext'] = array(
      '#type' => 'textfield',
      '#title' => t('jrbChart option'),
      '#required' => FALSE,
      '#maxlength' => 1024,
      '#size' => 600,
      '#default_value' => $this->options['format']['classtext'],
      '#description' => t('Enter text for jrbChartType. Table class will become "jrbChartType-<b>chart_options</b>" where <b>chart_options</b> is entered text. Click <a href="http://jrbcharts.com/content/chart-options" target="_new">here</a> to open a new window for details on the <b>chart_options</b> parameter.'),
    );

    $form['format']['headerrows'] = array(
      '#type' => 'textfield',
      '#title' => t('Header Rows'),
      '#required' => FALSE,
      '#default_value' => $this->options['format']['headerrows'],
      '#description' => t('Number of top rows to use as header.'),
    );

    $form['format']['headercols'] = array(
      '#type' => 'textfield',
      '#title' => t('Header Columns'),
      '#required' => FALSE,
      '#default_value' => $this->options['format']['headercols'],
      '#description' => t('Number of left columns to use as header.'),
    );

    $form['format']['makeeditable'] = array(
      '#type' => 'select',
      '#title' => t('Editable Values'),
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#options' => array('No', 'Yes'),
      '#default_value' => $this->options['format']['makeeditable'],
      '#description' => t('If set to "Yes", will make fields editable inline. Also set permissions - user must be in group enabled with "jrbchartsview editing"'),
    );

    $form['format']['use_canvas'] = array(
      '#type' => 'select',
      '#title' => t('Use Canvas'),
      '#required' => FALSE,
      '#multiple' => FALSE,
      '#options' => array('No', 'Yes'),
      '#default_value' => $this->options['format']['use_canvas'],
      '#description' => t('Select No to preserve the HTML table format. jrbChart option is ignored. Yes means that the HTML table is converted to a cavnas redering using jrbCharts and the jrbChart option above.'),
    );

    $form['sticky'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable Drupal style "sticky" table headers (Javascript)'),
      '#options' => array(0, 1),
      '#default_value' => $this->options['sticky'],
      '#description' => t('Ignored for canvas. Also sticky header effects will not be active for preview below, only on live output.'),
    );

  }



  /**
   * Function reders HTML table to jrbChart based on provided view parameters.
   */

  function render() {

    $htmlOut = "";
    $sets = $this->render_grouping($this->view->result, $this->options['grouping']);
    $keys = array_keys($this->view->field);
    $headrows = $this->options['format']['headerrows']; // number of top header rows
    $freezecols = $this->options['format']['headercols']; // number of left side columns
    $usecanvas = $this->options['format']['use_canvas']; // treat as TRUE or FALSE
	$this->view->usecanvas = $usecanvas;
    $header = $this->display->handler->get_field_labels();
    $first = 1;
    $posthtml = array();

    foreach ($sets as $title => $records) {
      $htmlOut .= $title;
      $rows = array();
      $tc = 1;
      $editcount = 0;

      foreach ($records as $c => $record) {

        foreach ($keys as $id) {
          if ($tc == 1) {
            $rows[0][$id] = $header[$id]; // field names are all in the first [0] row
          }
          $rows[$tc][$id] = $this->view->field[$id]->theme($record);
          $rows[$tc][$id] = str_replace("[empty]", "", $rows[$tc][$id]);

        }

        // determine which records (rows) a user can edit. 
        if ($this->view->jrbCharts_makeeditable) {
          if ($canedit = menu_get_item('node/' . $record->jrbcharts_uid . '/edit')) {
            if ($canedit['access']) {
              $rows[$tc]['jrbcharts_canedit'] = $record->jrbcharts_uid;
            }
            $editcount++;
          }
        }


        $tc++;
      }

      $htmlOut .= $this->_do_theme_table($rows, $headrows, $freezecols, $usecanvas, $posthtml, $editcount);

    }

    foreach ($posthtml as $k => $line) {
      $htmlOut .= $line;
    }

    return $htmlOut;
  } // end function render()



  // return HTML to render table and optionally jrbChart
  //   $rows_all - table of data
  //   $headrows - number of header top rows
  //   $freezecols - number of left header columns
  //   $usecanvas - any value means true
  function _do_theme_table(&$rows_all, $headrows, $freezecols, $usecanvas, &$posthtml, $editcount) {

    $classString = 'views-table';
    if ($usecanvas) {
      $classString .= ' jrbChartType-' . $this->options['format']['classtext'];
    }

    if (!$usecanvas && $this->options['sticky']) {
      drupal_add_js('misc/tableheader.js');
      $classString .= " sticky-enabled";
    }


    $htmlOut = '<table class="' . $classString . '"><thead>';
    $header = $rows_all[0];
    $blanketaccess = user_access('jrbchartsview editing');


    foreach ($rows_all as $count => $row) {
      if ($count == $headrows) {
        $htmlOut .= '</thead></tbody>';
      }

      $htmlOut .= '<tr class="';
      if ($count % 2) {
        $htmlOut .= 'odd';
      }
      else {
        $htmlOut .= 'even';
      }
      $htmlOut .= '">';

      $rc = 0;
      $useid = 0;
      $groupcache = array();

      if ($blanketaccess) {
        $useid = $row['jrbcharts_canedit'];
      }

      if (isset($row['jrbcharts_canedit'])) {
        unset($row['jrbcharts_canedit']);
      }

      foreach ($row as $field => $content) {

        if (!$count && $editcount && $blanketaccess) { // look for dropdown lists

          $fieldopts = array(
            'prefix' => $this->view->field[$field]->content_field['prefix'],
            'suffix' => $this->view->field[$field]->content_field['suffix'],
            'min' => $this->view->field[$field]->content_field['min'],
            'max' => $this->view->field[$field]->content_field['max'],
            'required' => $this->view->field[$field]->content_field['required'],
            'multiple' => $this->view->field[$field]->content_field['multiple'],
            'widget' => $this->view->field[$field]->content_field['widget']['type'],
            'size' => $this->view->field[$field]->content_field['widget']['size'],
            'rows' => $this->view->field[$field]->content_field['widget']['rows']
          );

          $allowedvals = $this->view->field[$field]->content_field['allowed_values'];

          if ($allowedvals) {
            $fieldopts['allowedvals'] = 1;
          }

          $classopts = '';
          foreach ($fieldopts as $optf => $optv) {
            if (isset($optv)) {
              if ($classopts) {
                $classopts .= '-';
              }
              $classopts .= $optf . '-' . $optv;
            }
          }

          if ($allowedvals) {

            $allowedvals = explode(PHP_EOL, $allowedvals);
            $posthtml[$field] = '<div class="jrbChartEditOpt ' . $field . ' ' . $classopts . '">';
            foreach ($allowedvals as $ddk => $ddv) {
              $ddv = preg_replace('/[^ -~]/', '', $ddv); // i.e. remove all non printable characters, stray newlines for example
              $posthtml[$field] .= '<span>' . $ddv . '</span>';
            }
            $posthtml[$field] .= '</div>';
          }
          else {
            if ($classopts) {
              $posthtml[$field] = '<div class="jrbChartEditOpt ' . $field . ' ' . $classopts . '"></div>';
            }
          }


        }

        if (!$this->view->field[$field]->options['exclude']) {

          if ($count < $headrows) {
            if (!$count && $this->view->field[$field]->content_field['widget']['description']) {
              $htmlOut .= '<th title="' . $this->view->field[$field]->content_field['widget']['description'] . '">' . $content . '</th>';
            }
            else {
              $htmlOut .= '<th>' . $content . '</th>';
            }
          }
          else {

            if ($useid) { //i.e. user has blanket access, and able to edit this row

              $content = _jrbchartsview_cellformat($this->view, $field, $content, $useid);

            }

            if ($rc < $freezecols) {
              $htmlOut .= '<th >' . $content . '</th>';
            }
            else {
              $htmlOut .= '<td >' . $content . '</td>';
            }

          }
          $rc++;
        }
      }
      $htmlOut .= '</tr>';
    }
    $htmlOut .= '</tbody></table>';

    $me = variable_get('jrbchartsview_libraryloc', ''); //base_path().conf_path().variable_get('jrbchartsview_libraryloc', '/libraries/jrbCharts');
    if (!$me) {
      $me = _get_path('jrbCharts');
      variable_set('jrbchartsview_libraryloc', $me);
    }

    if ($usecanvas) { // don't use HTML - use jrbCharts to present on a canvas

      if (variable_get('jrbchartsview_mode', 0)) { // self hosted

        if (file_check_location('jrbCharts.css', $me)) {

          $posthtml['scripts'] = "
<script type='text/javascript'>

if (!fileCSS) {
	var fileCSS=document.createElement('link');
	fileCSS.setAttribute('rel', 'stylesheet');
	fileCSS.setAttribute('type','text/css');
	fileCSS.setAttribute('href', '$me/jrbCharts.css');
	document.getElementsByTagName('head')[0].appendChild(fileCSS);
}
	
if (!fileCSSSup) {
	var fileCSSSup=document.createElement('link');
	fileCSSSup.setAttribute('rel', 'stylesheet');
	fileCSSSup.setAttribute('type','text/css');
	fileCSSSup.setAttribute('href', '$me/jrbChartsSup.css');
	document.getElementsByTagName('head')[0].appendChild(fileCSSSup);
}
	
if (!fileCanvas) {
	var fileCanvas=document.createElement('script');
	fileCanvas.setAttribute('type','text/javascript');
	fileCanvas.setAttribute('src', '$me/jrbCanvas.c.js');
	document.getElementsByTagName('head')[0].appendChild(fileCanvas);
}

if (!fileCharts) {
	var fileCharts=document.createElement('script');
	fileCharts.setAttribute('type','text/javascript');
	fileCharts.setAttribute('src', '$me/jrbCharts.c.js');
	document.getElementsByTagName('head')[0].appendChild(fileCharts);
}

if (!fileChartsHook) {
	var fileChartsHook=document.createElement('script');
	fileChartsHook.setAttribute('type','text/javascript');
	fileChartsHook.setAttribute('src', '$me/jrbCharts_hooks.js');
	document.getElementsByTagName('head')[0].appendChild(fileChartsHook);
}

function waittilload_(n) {

	if (n>100) return;
	
	if(typeof jrbCharts == 'function') {
		jrbCharts();
	} else {
		n++;
		var t=setTimeout('waittilload_('+n+')',100);
	}
}

waittilload_(0);

</script>";
        }
        else {
          drupal_set_message(check_plain("Warning: can't find chart scripts at $me. Check settings at /admin/settings/jrbchartsview."), 'error');
          $posthtml = array();
        }

      }
      else { //cloud
        $posthtml['scripts'] = '<script type="text/javascript" src="http://jrbcharts.com/jss"></script>';
      }

    }

    if ($editcount) {

      if (file_check_location('jrbchartsview.css', $me)) {

        $posthtml['scripts2'] = "
<script type='text/javascript'>

if (!fileviewCSS) {
	var fileviewCSS=document.createElement('link');
	fileviewCSS.setAttribute('rel', 'stylesheet');
	fileviewCSS.setAttribute('type','text/css');
	fileviewCSS.setAttribute('href', '$me/jrbchartsview.css');
	document.getElementsByTagName('head')[0].appendChild(fileviewCSS);
}

if (!filechartview_hooks) {
	var filechartview_hooks=document.createElement('script');
	filechartview_hooks.setAttribute('type','text/javascript');
	filechartview_hooks.setAttribute('src', '$me/jrbchartsview_hooks.js');
	document.getElementsByTagName('head')[0].appendChild(filechartview_hooks);
}

function waittilload_cvm(n) {

	if (n>100) return;
	
	if(typeof jrbchartsviewmain == 'function') {
		jrbchartsviewmain();
	} else {
		n++;
		var t=setTimeout('waittilload_cvm('+n+')',100);
	}
}

if (!filechartview) {
	var filechartview=document.createElement('script');
	filechartview.setAttribute('type','text/javascript');
	filechartview.setAttribute('src', '$me/jrbchartsview.c.js');
	document.getElementsByTagName('head')[0].appendChild(filechartview);
} else waittilload_cvm(0);

</script>";
      }
      else {
        drupal_set_message(check_plain("Warning: can't find edit scripts at $me. Check settings at /admin/settings/jrbchartsview."), 'error');
        $posthtml = array();
      }
      // add jrbchartsview.css
    }
    return $htmlOut;

  } // end function _do_theme_table

} // end class

/**
 * Gets the path of a library.
 */
function _get_path($name, $base_path = FALSE) {
  static $libraries;

  if (!isset($libraries)) {
    $libraries = _get_libraries();
  }

  $path = ($base_path ? base_path() : '');
  if (!isset($libraries[$name])) {
    // Most often, external libraries can be shared across multiple sites, so
    // we return sites/all/libraries as the default path.
    $path .= 'sites/all/libraries/' . $name;
  }
  else {
    $path .= $libraries[$name];
  }

  return base_path() . $path;
}

/**
 * Returns an array of library directories.
 */
function _get_libraries() {
  global $profile;

  if (!isset($profile)) {
    $profile = variable_get('install_profile', 'default');
  }

  $directory = 'libraries';
  $searchdir = array();
  $config = conf_path();

  $searchdir[] = $directory;

  if (file_exists("profiles/$profile/$directory")) {
    $searchdir[] = "profiles/$profile/$directory";
  }

  $searchdir[] = 'sites/all/' . $directory;

  if (file_exists("$config/$directory")) {
    $searchdir[] = "$config/$directory";
  }

  $directories = array();
  $nomask = array('CVS');
  foreach ($searchdir as $dir) {
    if (is_dir($dir) && $handle = opendir($dir)) {
      while (FALSE !== ($file = readdir($handle))) {
        if (!in_array($file, $nomask) && $file[0] != '.') {
          if (is_dir("$dir/$file")) {
            $directories[$file] = "$dir/$file";
          }
        }
      }
      closedir($handle);
    }
  }

  return $directories;
}
