****************************************************************************
*                   README for jrbchartsview Drupal module                 *
*                          Provided by jrbCharts.com                       *
****************************************************************************

-- SUMMARY --

The jrbchartsview module provides both charting and inline editing capabilities. Specifically 
it enhances DRUPAL with the following functions
- incorporation of canvas jrbCharts library from jrbCharts.com
- inline editing. An administrator can bulk edit values as displayed in an HTML table 
  or canvas chart directly without having to open up a form for each record. 

  
For a full description of the module, visit the project page:
   http://drupal.org/project/1880492

   
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/1880492
  
-- REQUIREMENTS --

* This module was developed for a site using Views2 and Drupal6. A Drupal 7 version is in development.
* Client devices must be capable of HTML5/Canvas to use the charting capabilities.

You do not need to obtain the javascript libraries from jrbCharts.com. jrbCharts provides a service 
to provide scripts for you. 
  
-- INSTALLATION --
 
* Install module in a /modules subdirectory. E.g. /sites/all/modules.

* If libraries are obtained from jrbCharts.com, install in and /libraries location. E.g. /sites/all/libraries.
 
-- CONFIGURATION --

* Permissions need to be set for inline editing. Look for 'jrbchartsview editing' at /admin/user/permissions 
  and select groups as desired. 
  
* Administrative settings can be found at /admin/settings/jrbchartsview. The module works out of the box
  with default settings. The 'cloud' option will be used and javascript libraries will be obtained from 
  jrbCharts.com. 
  
  Two operational modes are provided: Cloud and Self Hosted. Cloud means that javascript, css files 
  and themes are downloaded from jrbCharts.com whenever the charts are rendered. The advantage is that 
  the latest version is always used. The disadvantage is that the ability to make your own modifications 
  to css styles and themes are not available. Also - and this is important for operational sites - you are 
  dependent on the jrbCharts.com server. Self hosted means that you host the javascript, css, and themes 
  yourself. You are able to modify the css and themes as you wish. However updates to the scripts are not 
  automatic. If you select Self Hosted, place all downloaded .js and .css files in a /libraries/jrbCharts 
  location. All files must be in this directory. Theme images should be in subdirectories to this location.

-- USE --

* Setup a chart in views. Under Basic Settings, a 'jrbCharts view' option becomes available (in addition
  to Table, Grid, Unformatted, etc...)
* Under settings for 'jrbCharts view' select options as follows:
   - jrbChart option. This is a text string that defines the chart. Leave blank for an interactive table. 
     Or it can be as simple as 'bar', 'shape' where X data is in the 1st column and Y data in the subsequent 
	 columns to create simple interactive charts. Or much more complex constructs can be created. See 
	 'http://jrbcharts.com/content/chart-options' for more.
   - Header Rows and Header Columns specify the number of table top rows and left columns to be treated
     as header data. This data will be used as labels rather than data to be plotted.
   - Editable values - select 'Yes' to enable inline editing. Also don;t forget to set permissions - a user 
     must be in group enabled with "jrbchartsview editing" to use this feature.
   - Use canvas - select 'Yes' to use canvas. Else an HTML table is created. Note that inline editing is 
     available with or without use of Canvas   


 -- CUSTOMIZATION --
 
 Charts can be customized with CSS styles. See http://jrbcharts.com/content/themes-css for examples. Copy from 
 jrbCharts.css as needed. 
 
 -- CONTACT --
 
 Current maintainers:
 * John R Bell - http://drupal.org/user/1827774
 
 * jrbCharts.com - Use the contact form at http://jrbcharts.com/contact
