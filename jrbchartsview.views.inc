<?php
/**
 * @file
 * jrbCharts Views Integration
 */

/**
 * Implementation of hook_views_plugins().
 *
 * Define jrbchartsview style for Views.
 */
function jrbchartsview_views_plugins() {

  return array(
    'module' => 'jrbchartsview',
    'style' => array(// Declare the jrbchartsview style plugin
      'jrbchartsview' => array(
        'handler' => 'jrbchartsview_views_plugin_style_chart',
        'help' => t('jrbCharts presentation style.'),
        'path' => drupal_get_path('module', 'jrbchartsview'),
        'parent' => 'parent',
        'theme' => 'views_view_unformatted',
        'title' => t('jrbCharts view'),
        'type' => 'normal',
        'uses fields' => TRUE,
        'uses options' => TRUE,
      ),
    ),
  );
}
